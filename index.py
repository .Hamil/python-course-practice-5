#------------ EJERCICIO 1 - SIN USAR SQLALCHEMY ORM ------------
#dependencias: mysql-connector-python
#instalacion: pip install mysql-connector-python

import mysql.connector
from mysql.connector import Error

try:
    connection = mysql.connector.connect(host='localhost',
                                        database='practica_5',
                                        user='root',
                                        password='')
    if connection.is_connected():
        db_Info = connection.get_server_info()
        print("Esta conectado a la version de MySQL ", db_Info)
        cursor = connection.cursor()
        cursor.execute("select database();")
        record = cursor.fetchone()
        print("Te conectaste a la base de datos: ", record)

except Error as e:
    print("Error al conectarnos a MySQL", e)
finally:
    if (connection.is_connected()):
        cursor.close()
        connection.close()
        print("La conexion de MySQL esta cerrada")

#------------ EJERCICIO 1 - USANDO SQLALCHEMY ORM ------------


import sqlalchemy as db
from sqlalchemy.ext.declarative import declarative_base

engine = db.create_engine(
    'mysql+mysqlconnector://root:@localhost:3306/practica_5',
    echo=True)

#a la hora de realizar la conexion, hay que especificar el driver que usaremos (dialecto + driver://usuario:pass@host:puerto/baseDeDatos

conn = engine.connect()
metadata = db.MetaData()
categories = db.Table('categories', metadata, autoload=True, autoload_with=engine)

query_select = db.select([categories])
results = conn.execute(query_select).fetchall()
print(categories.columns.keys())
for result in results:
    print(result)

#------------ EJERCICIO 2 - SIN USAR SQLALCHEMY ORM ------------
#dependencias: cx_Oracle
#instalacion: pip install cx_Oracle


import cx_Oracle
dsn_tns = cx_Oracle.makedsn('server', 'port', 'sid')
conn_oracle = cx_Oracle.connect(user='username', password='password', dsn=dsn_tns)
c = conn.cursor()
c.execute('select * from TABLE_NAME')
for row in c:
   print(row)
conn_oracle.close() #cerramos la conexion

#------------ EJERCICIO 2 - CON SQLALCHEMY ORM ------------
dsn = cx_Oracle.makedsn(
    '192.168.1.105', 49161,
    # service_name='your_service_name_if_any'
)
oracle_engine = create_engine(f'oracle+cx_oracle://{user}:{pwd}@{dsn}', echo=True)
oracle_engine.connect()


#------------ EJERCICIO 3 - SIN USAR SQLALCHEMY ORM ------------
#dependencias: pyodbc
#instalacion: pip instal pyodbc

import pyodbc 
conn = pyodbc.connect('Driver={SQL Server};'
                      'Server=server_name;'
                      'Database=database_name;'
                      'Trusted_Connection=yes;')

cursor = conn.cursor()
cursor.execute('SELECT * FROM database_name.table')

for row in cursor:
    print(row)

#------------ EJERCICIO 3 - CON SQLALCHEMY ORM ------------
engine = db.create_engine('mssql+pyodbc://server/database')
